class Player:
    def play_turn(self, board):
        raise NotImplementedError()

    def end_game(self):
        raise NotImplementedError()
