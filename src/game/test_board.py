import pytest
from game.board import Board


def test_can_play():
    w, h = 7, 6
    b = Board(h, w)

    assert not b.can_play(-1)
    assert not b.can_play(7)

    for _ in range(h):
        assert b.can_play(2)
        b.play(2, 1)

    assert not b.can_play(2)
