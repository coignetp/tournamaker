from game.board import Board

# This is a game example: connect 4
# Do not change the classname


class Game:
    def __init__(self):
        self.players = []
        self.actual_player = 1
        self.board = Board(7, 9)

    def play_turn(self):
        col = self.players[self.actual_player - 1].play_turn(self.board)

        return self.board.play(col, self.actual_player)

    def start(self, players):
        winner = 0
        self.players = players

        while winner == 0:
            ret = self.play_turn()
            if not ret:
                print(f"Player {self.actual_player} made unauthorized move")

            self.actual_player = (self.actual_player % 2) + 1
            winner = self.board.get_winner()

        return winner, self.board.action
