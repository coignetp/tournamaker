class Board:
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.board = [[0 for _ in range(height)] for _ in range(width)]
        self.action = []

    def get_player(self, x, y):
        return self.board[x][y]

    def can_play(self, column):
        return column >= 0 and column < self.width and self.board[column][-1] == 0

    def play(self, column, id):
        for i in range(0, self.height):
            if self.board[column][i] == 0:
                self.action.append((column, i))
                self.board[column][i] = id
                return True

        self.action.append((-1, -1))
        return False

    def undo(self):
        if len(self.action) > 0:
            self.board[self.action[-1][0]][self.action[-1][1]] = 0
            self.action.pop()

            return True
        return False

    def get_column_size(self, x, y):
        i = 1

        while y + i < self.height:
            if self.board[x][y] != self.board[x][y + i]:
                return i
            i += 1

        return i

    def get_line_size(self, x, y):
        i = 1

        while x + i < self.width:
            if self.board[x][y] != self.board[x + i][y]:
                return i
            i += 1

        return i

    def get_diag_up_right_size(self, x, y):
        i = 1

        while x + i < self.width and y + i < self.height:
            if self.board[x][y] != self.board[x + i][y + i]:
                return i
            i += 1

        return i

    def get_diag_down_right_size(self, x, y):
        i = 1

        while x + i < self.width and y - i >= 0:
            if self.board[x][y] != self.board[x + i][y - i]:
                return i
            i += 1

        return i

    def get_winner(self):
        draw = True

        for x in range(self.width):
            for y in range(self.height):
                if self.board[x][y] != 0 and (
                    self.get_column_size(x, y) >= 4
                    or self.get_line_size(x, y) >= 4
                    or self.get_diag_up_right_size(x, y) >= 4
                    or self.get_diag_down_right_size(x, y) >= 4
                ):
                    return self.board[x][y]
                if self.board[x][y] == 0:
                    draw = False
        if draw:
            return -1
        return 0

    def __str__(self):
        str = ""
        for j in range(self.height):
            str += "\n|"
            for i in range(self.width):
                str += f" {self.board[i][-1-j]} |"

        return str
