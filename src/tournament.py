import subprocess
import shutil
import docker
import round
import time
import argparse
import os


class Tournament:
    def __init__(self):
        self.path = "players"
        self.extension = ".py"
        self.mod = [
            self.path + "." + f[:-3] for f in os.listdir(self.path) if f[:2] != "__"
        ]

    def start(self, host="http://localhost:4280", token="test", rnd=1, max_docker=10):
        client = docker.from_env()

        for i in range(len(self.mod) - 1):
            for j in range(i + 1, len(self.mod)):
                while len(client.containers.list()) >= max_docker:
                    time.sleep(1)
                os.system(
                    f'sudo docker run -d --rm --name {self.mod[i].split(".")[-1]}_vs_{self.mod[j].split(".")[-1]} --network host'
                    f' -v "$PWD":/app -w /app  --entrypoint=python broadinstitute/python-requests round.py --token {token} --rnd {rnd} --players {self.mod[i]} {self.mod[j]} --host "{host}"'
                )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--token", type=str, help="token of the tournament", required=True
    )
    parser.add_argument("--rnd", type=int, help="The number of round to do", default=1)
    parser.add_argument(
        "--maxdocker",
        type=int,
        help="The maximum number of docker that can be run at the same time",
    )
    args = parser.parse_args()

    t = Tournament()
    t.start(token=args.token, rnd=args.rnd, max_docker=args.maxdocker)
