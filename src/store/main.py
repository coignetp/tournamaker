from flask import Flask, g

from storage import get_data, save_data

# The flask application. Must be global according to Flask design.
StoreApp = Flask(__name__)

from routes.round import *
from routes.general import *


if __name__ == "__main__":
    StoreApp.logger.debug("Loading data....")
    StoreApp.config.update(DB_JSON=get_data())
    # Running the store application
    StoreApp.run(host="0.0.0.0", port=4280, debug=True)

    StoreApp.logger.debug("Saving data....")
    save_data(StoreApp.config["DB_JSON"])
