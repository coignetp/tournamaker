from main import StoreApp
from flask import request, g
import json


@StoreApp.route("/round/<int:id>", methods=["GET"])
def get_round(id):
    body = request.get_json()

    token = body.get("token", None)

    if token is None or not token in StoreApp.config["DB_JSON"]:
        return "Bad or no tournament token", 403

    if id < 0 or id >= len(StoreApp.config["DB_JSON"][token]):
        return "Bad round id", 401

    return json.dumps(StoreApp.config["DB_JSON"][token][id])


@StoreApp.route("/round", methods=["GET"])
def get_all_rounds():
    body = request.get_json()

    token = body.get("token", None)

    if token is None:
        return "No tournament token", 403

    rounds = StoreApp.config["DB_JSON"].get(token, None)

    return json.dumps(rounds)


@StoreApp.route("/round", methods=["POST"])
def create_round():
    body = request.get_json()

    token = body.get("token", None)
    new_round = body.get("round", None)

    if token is None:
        return "No tournament token", 403
    if new_round is None:
        return "No round to insert", 400

    if not token in StoreApp.config["DB_JSON"]:
        StoreApp.config["DB_JSON"][token] = []

    StoreApp.config["DB_JSON"][token].append(new_round)

    return str(len(StoreApp.config["DB_JSON"][token]))
