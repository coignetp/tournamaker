from main import StoreApp
from flask import request, g
from storage import save_data
import json


@StoreApp.route("/save", methods=["GET"])
def save_persistent():
    save_data(StoreApp.config["DB_JSON"])

    return "OK"
