import os
import json


def save_data(db: dict):
    print("Start saving")
    for token, data in db.items():
        with open("store/data/{}.json".format(token), "w+") as file:
            json.dump(data, file)


def get_data():
    filenames = os.listdir("store/data/")
    db = {}

    for filename in filenames:
        if filename.endswith(".json"):
            token = filename.split(".")[0]
            with open(os.path.join("store/data", filename)) as file:
                db[token] = json.load(file)
    return db
