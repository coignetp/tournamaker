import importlib
from game.game import Game
import argparse
import json
import requests


def play_round(file1, file2, token, url):
    play1 = importlib.import_module(file1)
    play2 = importlib.import_module(file2)

    p1 = play1.AIPlayer(1)
    p2 = play2.AIPlayer(2)

    g = Game()
    w, actions = g.start([p1, p2])

    body = {
        "token": token,
        "round": {"players": [file1, file2], "winner": w, "actions": actions},
    }

    requests.post(url, json=body)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", type=str, help="The host of the store", required=True)
    parser.add_argument(
        "--token", type=str, help="The token of the tournament", required=True
    )
    parser.add_argument("--rnd", type=int, help="The number of round to do", default=1)
    parser.add_argument("--players", nargs="+", help="The list of AI", required=True)
    args = parser.parse_args()

    host = args.host
    token = args.token

    url = "{}/round".format(host)
    print("## Endpoint: {}".format(url))

    if len(args.players) < 2:
        raise ValueError()

    p1 = args.players[0]
    p2 = args.players[1]

    for _ in range(args.rnd):
        play_round(p1, p2, token, url)

        play_round(p2, p1, token, url)
