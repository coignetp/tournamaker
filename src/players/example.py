from model.player import Player
import random


class AIPlayer(Player):
    def __init__(self, id):
        pass

    def play_turn(self, board):
        return random.randint(0, 8)

    def end_game(self):
        pass
