from model.player import Player
from game.board import Board


class Human(Player):
    def __init__(self, id):
        self.id = id

    def play_turn(self, board):
        print(board)
        col = int(input("Choose column (0 to 8): "))

        return col

    def end_game(self):
        pass


# Callable
AIPlayer = Human
