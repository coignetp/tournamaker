#!/bin/bash

cd "$(dirname "$0")"
cd src

sudo docker run -d --name tournamaker_store -p "4280:4280" \
    -v "$PWD/store":"/src/store" \
    -w /src --entrypoint=python jcdemo/flaskapp store/main.py
    
while true; do
    curl localhost:4280/save &
done
